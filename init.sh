#!/bin/bash

set -e


if [ "$1" == "no-x" ]; then
  tasks=(
    rust
    kr
    zsh
  )
else
  tasks=(
    rust
    kr
    vscode
    terminator
    zsh
  )
fi

for task in ${tasks[@]}; do
  if [ -f "./${task}/init.sh" ]; then
    ./${task}/init.sh
  else
    echo "No init script for task $task"
  fi
done
