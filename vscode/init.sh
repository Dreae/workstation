#!/bin/bash

set -e

sudo rpm --import https://packages.microsoft.com/keys/microsoft.asc
sudo sh -c 'echo -e "[code]\nname=Visual Studio Code\nbaseurl=https://packages.microsoft.com/yumrepos/vscode\nenabled=1\ngpgcheck=1\ngpgkey=https://packages.microsoft.com/keys/microsoft.asc" > /etc/yum.repos.d/vscode.repo'

set +e
dnf check-update
set -e

sudo dnf -y install code

code --install-extension dracula-theme.theme-dracula
code --install-extension ms-vscode.cpptools
code --install-extension dreae.sourcepawn-vscode
code --install-extension rust-lang.rust
code --install-extension karunamurti.tera
code --install-extension wayou.vscode-todo-highlight
code --install-extension minhthai.vscode-todo-parser