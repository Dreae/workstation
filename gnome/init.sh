#!/bin/bash

set -e

sudo dnf -y install arc-theme
gsettings set org.gnome.desktop.interface gtk-theme 'Arc-Darker'
