#!/bin/bash

set -e

sudo dnf -y install zsh

git clone https://github.com/robbyrussell/oh-my-zsh.git /tmp/oh-my-zsh
cd /tmp/oh-my-zsh/tools
./install.sh

sed -i'' s/^ZSH_THEME=\".*\"/ZSH_THEME=\"norm\"/ ~/.zshrc
