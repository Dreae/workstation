#!/bin/bash

set -e

sudo dnf -y install terminator
mkdir -p ~/.config/terminator
cp terminator/config ~/.config/terminator/config
