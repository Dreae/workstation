#!/bin/bash

set -e

sudo dnf -y install go

go get github.com/kryptco/kr
cd ~/go/src/github.com/kryptco/kr

source ~/.cargo/env
make
make install
